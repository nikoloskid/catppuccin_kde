#!/usr/bin/env bash

# Syntax <Flavour = 1-4 > <Accent = 1-14> <WindowDec = 1/2> <Debug = global/color/splash/cursor>

COLORDIR=~/.local/share/color-schemes
AURORAEDIR=~/.local/share/aurorae/themes
LOOKANDFEELDIR=~/.local/share/plasma/look-and-feel
DESKTOPTHEMEDIR=~/.local/share/plasma/desktoptheme
CURSORDIR=~/.local/share/icons

echo "Creating theme directories.."
mkdir -p $COLORDIR
mkdir -p $AURORAEDIR
mkdir -p $LOOKANDFEELDIR
mkdir -p $DESKTOPTHEMEDIR
mkdir -p $CURSORDIR
mkdir ./dist

# Fast install
FLAVOUR="$1"
ACCENT="$14"
WINDECSTYLE="$1"

DEBUGMODE="$4"

clear

if [[ "$1" == "" ]]; then
    FLAVOUR="1"
fi

if [[ $FLAVOUR == "1" ]]; then
    echo "The pallet Mocha(1) was selected";
    FLAVOURNAME="Mocha";
elif [[ $FLAVOUR == "2" ]]; then
    echo "The pallet Macchiato(2) was selected";
    FLAVOURNAME="Macchiato";
elif [[ $FLAVOUR == "3" ]]; then
    echo "The pallet Frappe(3) was selected";
    FLAVOURNAME="Frappe";
elif [[ $FLAVOUR == "4" ]]; then
    echo "The pallet Latte(4) was selected";
    FLAVOURNAME="Latte";
else echo "Not a valid flavour name" && exit;
fi
echo ""

if [[ "$2" == "" ]]; then
    ACCENT="14"
    clear
fi

# Sets accent based on the pallet selected (Best to fold this in your respective editor)
if [[ $ACCENT == "14" ]]; then
    ACCENTCOLOR="180,190,254"
    echo "Accent Lavender(14) was selected!"
    ACCENTNAME="Lavender"
else
    echo "Not a valid accent" && exit
fi

GLOBALTHEMENAME="Catppuccin-$FLAVOURNAME-$ACCENTNAME"

if [[ "$3" == "" ]]; then
    WINDECSTYLE="1"
    clear
fi

if [[ $WINDECSTYLE == "1" ]]; then
    WINDECSTYLENAME=Modern
    WINDECSTYLECODE=__aurorae__svg__Catppuccin"$FLAVOURNAME"-Modern
    echo "Hey! thanks for picking 'Modern', this one has a few rules or else it might break
 1: Use 3 icons on the right, With the 'Close' Button on the Far-Right
 2: If you would like the pin on all desktops button, You need to place it on the left."
    echo "We apologize if you wanted a different configuration :("
    sleep 2
elif [[ $WINDECSTYLE == "2" ]]; then
    WINDECSTYLENAME=Classic
    WINDECSTYLECODE=__aurorae__svg__Catppuccin"$FLAVOURNAME"-Classic
else
    echo "Not a valid Window decoration"
fi

function ModifyLightlyPlasma {

    rm -rf $DESKTOPTHEMEDIR/lightly-plasma-git/icons/*
    rm -rf $DESKTOPTHEMEDIR/lightly-plasma-git/translucent
    rm $DESKTOPTHEMEDIR/lightly-plasma-git/widgets/tabbar.svgz
    rm $DESKTOPTHEMEDIR/lightly-plasma-git/dialogs/background.svgz

    # Copy Patches
    cp $DESKTOPTHEMEDIR/lightly-plasma-git/solid/* $DESKTOPTHEMEDIR/lightly-plasma-git -Rf
    cp ./Patches/glowbar.svg $DESKTOPTHEMEDIR/lightly-plasma-git/widgets -rf
    cp ./Patches/background.svg $DESKTOPTHEMEDIR/lightly-plasma-git/widgets -rf
    cp ./Patches/panel-background.svgz $DESKTOPTHEMEDIR/lightly-plasma-git/widgets

    # Modify description to state that it has been modified by the KDE Catppuccin Installer
    sed -e s/A\ plasma\ style\ with\ close\ to\ the\ look\ of\ the\ newest\ Lightly./*MODIFIED\ BY\ CATPPUCCIN\ KDE\ INSTALLER*\ A\ plasma\ style\ with\ close\ to\ the\ look\ of\ the\ newest\ Lightly./g $DESKTOPTHEMEDIR/lightly-plasma-git/metadata.desktop >> $DESKTOPTHEMEDIR/lightly-plasma-git/newMetadata.desktop
    cp -f $DESKTOPTHEMEDIR/metadata.desktop $DESKTOPTHEMEDIR/lightly-plasma-git/metadata.desktop && rm $DESKTOPTHEMEDIR/metadata.desktop
}

function AuroraeInstall {

    if [[ $WINDECSTYLE == "1" ]]; then
        cp ./Resources/Aurorae/Catppuccin"$FLAVOURNAME"-Modern $AURORAEDIR -r
        if [[ $FLAVOUR = "4" ]]; then
            cp ./Resources/Aurorae/Common/CatppuccinLatte-Modernrc $AURORAEDIR/CatppuccinLatte-Modern/CatppuccinLatte-Modernrc
        else
            cp ./Resources/Aurorae/Common/Catppuccin-Modernrc $AURORAEDIR/Catppuccin"$FLAVOURNAME"-Modern/Catppuccin"$FLAVOURNAME"-Modernrc
        fi
    elif [[ $WINDECSTYLE == "2" ]]; then
        cp ./Resources/Aurorae/Catppuccin"$FLAVOURNAME"-Classic $AURORAEDIR -r
        if [[ $FLAVOUR = "4" ]]; then
            cp ./Resources/Aurorae/Common/CatppuccinLatte-Classicrc $AURORAEDIR/CatppuccinLatte-Classic/CatppuccinLatte-Classicrc
        else
            cp ./Resources/Aurorae/Common/Catppuccin-Classicrc $AURORAEDIR/Catppuccin"$FLAVOURNAME"-Classic/Catppuccin"$FLAVOURNAME"-Classicrc
        fi
    fi
}

function BuildColorscheme {

    # Add Metadata & Replace Accent in colors file
    sed -e s/--accentColor/$ACCENTCOLOR/g -e s/--flavour/$FLAVOURNAME/g -e s/--accentName/$ACCENTNAME/g ./Resources/Base.colors > ./dist/base.colors
    # Hydrate Dummy colors according to Pallet
    FLAVOURNAME=$FLAVOURNAME ./Installer/color-build.sh -o ./dist/Catppuccin$FLAVOURNAME$ACCENTNAME.colors -s ./dist/base.colors
}

function BuildSplashScreen {

    if [[ $FLAVOUR == "1" ]]; then
        MANTLECOLOR=#181825
    elif [[ $FLAVOUR == "2" ]]; then
        MANTLECOLOR=#1e2030
    elif [[ $FLAVOUR == "3" ]]; then
        MANTLECOLOR=#292c3c
    elif [[ $FLAVOUR == "4" ]]; then
        MANTLECOLOR=#e6e9ef
    fi

    # Hydrate Dummy colors according to Pallet
    FLAVOURNAME=$FLAVOURNAME ./Installer/color-build.sh -s ./Resources/Splash/images/busywidget.svg -o ./dist/$GLOBALTHEMENAME/contents/splash/images/_busywidget.svg
    # Replace Accent in colors file
    sed ./dist/"$GLOBALTHEMENAME"/contents/splash/images/_busywidget.svg -e s/REPLACE--ACCENT/$ACCENTCOLOR/g > ./dist/"$GLOBALTHEMENAME"/contents/splash/images/busywidget.svg
    # Cleanup temporary file
    rm ./dist/"$GLOBALTHEMENAME"/contents/splash/images/_busywidget.svg

    # Hydrate Dummy colors according to Pallet (QML file)
    sed -e s/REPLACE--MANTLE/"$MANTLECOLOR"/g ./Resources/Splash/Splash.qml > ./dist/"$GLOBALTHEMENAME"/contents/splash/Splash.qml
    # Add CTP Logo
    if [[ $FLAVOUR != "4" ]]; then
        cp ./Resources/Splash/images/Logo.png ./dist/"$GLOBALTHEMENAME"/contents/splash/images/Logo.png
    else
        cp ./Resources/Splash/images/Latte_Logo.png ./dist/"$GLOBALTHEMENAME"/contents/splash/images/Logo.png
    fi
}

function InstallGlobalTheme {

    # Prepare Global Theme Folder
    cp -r ./Resources/LookAndFeel/Catppuccin-"$FLAVOURNAME"-Global ./dist/"$GLOBALTHEMENAME"
    mkdir -p ./dist/"$GLOBALTHEMENAME"/contents/splash/images

    # Hydrate Metadata with Pallet + Accent Info
    sed -e s/--accentName/"$ACCENTNAME"/g -e s/--flavour/"$FLAVOURNAME"/g ./Resources/LookAndFeel/metadata.desktop > ./dist/Catppuccin-"$FLAVOURNAME"-"$ACCENTNAME/metada"ta.desktop

    # Modify 'defaults' to set the correct Aurorae Theme
    sed -e s/--accentName/"$ACCENTNAME"/g -e s/--flavour/"$FLAVOURNAME"/g -e s/--aurorae/"$WINDECSTYLECODE"/g ./Resources/LookAndFeel/defaults > ./dist/Catppuccin-"$FLAVOURNAME"-"$ACCENTNAME"/contents/defaults

    # Build SplashScreen
    echo "Building SplashScreen.."
    BuildSplashScreen

    # Install Global Theme.
    # This refers to the QDBusConnection: error: could not send signal to service error
    # Which has had no effect in our testing on the working of this Installer.

    echo "
 WARNING: There might be some errors that might not affect the installer at all during this step, Please advise.
    "
    sleep 1
    echo "Installing Global Theme.."
    cd ./dist && tar -cf "$GLOBALTHEMENAME".tar.gz "$GLOBALTHEMENAME"
    kpackagetool5 -i "$GLOBALTHEMENAME".tar.gz
    cd ..

    if [[ ! -d "$DESKTOPTHEMEDIR/lightly-plasma-git/" ]]; then
        clear
        echo ""
        echo "Installation failed, could not fetch the lightly plasma theme lightly-plasma-git from store.kde.org"
        echo "Here are some things you can do to try fixing this:"
        echo " 1: Rerunning the install script"
        echo " 2: Check your intenet connection"
        echo " 3: See if https://store.kde.org is blocked"
        echo " 4: Manually installing Lightly-Plasma from https://pling.com/p/1879921/"
        echo ""
        echo "Would you like to install Catppuccin/KDE without lightly plasma? [y/n]:"
        CONFIRMATION="y"  # Set confirmation to "yes"
        echo ""
        echo "Continuing without lightly plasma.."
    else
        echo "Modifying lightly plasma theme.."
        ModifyLightlyPlasma
    fi

}

function InstallColorscheme {
    echo "Building Colorscheme.."

    # Generate Color scheme
    BuildColorscheme

    # Install Colorscheme
    echo "Installing Colorscheme.."
    mv ./dist/Catppuccin"$FLAVOURNAME$ACCENTNAME".colors $COLORDIR
}

function GetCursor {
    # Fetches cursors
    echo "Downloading Catppuccin Cursors from Catppuccin/cursors..."
    sleep 1.5
    wget -P ./dist https://github.com/catppuccin/cursors/releases/download/v0.2.0/Catppuccin-"$FLAVOURNAME"-"$ACCENTNAME"-Cursors.zip
    wget -P ./dist https://github.com/catppuccin/cursors/releases/download/v0.2.0/Catppuccin-"$FLAVOURNAME"-Dark-Cursors.zip
    cd ./dist && unzip Catppuccin-"$FLAVOURNAME"-"$ACCENTNAME"-Cursors.zip
    unzip Catppuccin-"$FLAVOURNAME"-Dark-Cursors.zip
    cd ..
}

function InstallCursor {
    GetCursor
    mv ./dist/Catppuccin-"$FLAVOURNAME"-"$ACCENTNAME"-Cursors $CURSORDIR
    mv ./dist/Catppuccin-"$FLAVOURNAME"-Dark-Cursors $CURSORDIR
}

# Syntax <Flavour> <Accent> <WindowDec> <Debug = global/color/splash/cursor>
if [[ $DEBUGMODE == "" ]]; then
    echo ""
    echo "Install $FLAVOURNAME $ACCENTNAME? with the $WINDECSTYLENAME window Decorations? [y/n]:"
    CONFIRMATION="y"
    clear
elif [[ $DEBUGMODE == "global" ]]; then
    InstallGlobalTheme
    exit
elif [[ $DEBUGMODE == "color" ]]; then
    BuildColorscheme
    exit
elif [[ $DEBUGMODE == "splash" ]]; then
    # Prepare Global Theme Folder
    GLOBALTHEMENAME="Catppuccin-$FLAVOURNAME-$ACCENTNAME"

    cp -r ./Resources/LookAndFeel/Catppuccin-"$FLAVOURNAME"-Global ./dist/"$GLOBALTHEMENAME"
    mkdir -p ./dist/"$GLOBALTHEMENAME"/contents/splash/images

    BuildSplashScreen
elif [[ $DEBUGMODE == "cursor" ]]; then
    GetCursor
else
    echo "Invalid Debug Mode"
fi

if [[ $CONFIRMATION == "Y" ]] || [[ $CONFIRMATION == "y" ]]; then

    # Build and Install Global Theme
    InstallGlobalTheme

    # Build Colorscheme
    InstallColorscheme

    echo "Installing aurorae theme.."
    AuroraeInstall

    echo "Installing Catppuccin Cursor theme.."
    InstallCursor

    # Cleanup
    echo "Cleaning up.."
    rm -rf ./dist

    # Apply theme
    echo ""
    echo "Do you want to apply theme? [y/n]:"
    CONFIRMATION="y"

    if [[ $CONFIRMATION == "Y" ]] || [[ $CONFIRMATION == "y" ]]; then
        lookandfeeltool -a "$GLOBALTHEMENAME"
        clear
        echo "The cursors will fully apply once you log out"

        # Some legacy apps still look in ~/.icons
        echo "You may want to run the following in your terminal if you notice any inconsistencies for the cursor theme"
        echo "ln -s ~/.local/share/icons/ ~/.icons"
    else
        echo "You can apply theme at any time using system settings"
        sleep 0.5
    fi
else echo "Exiting.." && exit
fi
